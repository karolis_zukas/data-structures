/*
    Set

    Basically array, with no duplicate items.

    is already implemented in ES6
*/

class MySet {
    constructor(){
        this.collection = [];
    }

    has(element) {
        return (this.collection.indexOf(element) !== -1);
    }

    values() {
        return this.collection;
    }

    add(element) {
        if (!this.has(element)) {
            this.collection.push(element);
            return this.collection;
        }
        throw new Error('Element already exists');
    }

    remove(element) {
        if (this.has(element)) {
            index = this.collection.indexOf(element);
            this.collection.splice(index, 1);
            return this.collection;
        }
        throw new Error('Element does not exist');
    }

    size() {
        return this.collection.length;
    }

    union(otherSet) {
        let unionSet = new MySet();
        let firstSet = this.values();
        let secondSet = otherSet.values();

        firstSet.forEach(el => unionSet.add(el));
        secondSet.forEach(el => unionSet.add(el));

        return unionSet;
    }

    intersection(otherSet) {
        let intersectionSet = new MySet();
        let firstSet = this.values();

        firstSet.forEach(el => {
            if (otherSet.has(el)) {
                intersectionSet.add(e);
            }
        })

        return intersectionSet;
    }

    difference(otherSet) {
        let differenceSet = new MySet();
        let firstSet = this.values();

        firstSet.forEach(el => {
            if (!otherSet.has(el)) {
                differenceSet.add(el);
            }
        });

        return differenceSet;
    }

    subset(otherSet) {
        let firstSet = this.values();
        return firstSet.every(el => otherSet.has(value));
    }
}