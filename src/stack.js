/* Stacks */

/* Usage: Mostly backtracing - if we require getting back to previous state (Think redux),
    we need to store state as stack. Event queus (think JS events), use same process.

    You can use array as a stack in js, because we have all the methods needed;
*/

/* functions: push() pop(), peek(), length() */

let letters = [];
let word = 'racecar';
let reverseWord = '';

// Put letters to stack
for (let i = 0; i < word.length; i++) {
    letters.push(word[i]);  // Push to stack
}

// Pop off stack to reverse order
for (let i = 0; i < word.length; i++) {
    reverseWord += letters.pop(); // Pop from stack
}

if (word === reverseWord) {
    console.log('palindrome');
}

/* diy Implementation */

class Stack {
    constructor() {
        this.count = 0;
        this.storage = {};
    }

    push(element) {
        this.storage[this.count] = value;
        this.count++;
    }

    pop() {
        if(this.count === 0) {
            return undefined;
        }

        this.count--;
        let result = this.storage[this.count];
        delete this.storage[this.count];

        return result;
    }

    size() {
        return this.count;
    }

    peek() {
        return this.storage[this.count-1];
    }
}

let test = new Stack();
test.push(1);
test.push(2);

console.log(test.peek());
console.log(test.pop());
console.log(test.peek());
console.log(test.size());
test.push('aaaa');
test.push(test.peek());